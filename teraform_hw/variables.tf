variable "yandex_cloud_token" {
  type = string
  description = "Данная переменная потребует ввести секретный токен в консоли при запуске terraform plan/apply"
  sensitive = true
}

variable "cloud_id" {
  type = string
  description = "Идентификатор облака"
}

variable "folder_id" {
  type = string
  description = "Идентификатор группы хостов"
  
}

variable "cpu" {
  type = number
  description = "Количество ядер"
  
}

variable "mem" {
  type = number
  description = "Объем оперативнойпамяти"
  
}

variable "zone_of_dc" {
  type = string
  description = "Зона расположения датацентра"
  
}

variable "image_id" {
  type = string
  description = "Идентификатор дистрибутива"
}